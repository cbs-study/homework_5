# Створити клас Contact з полями surname, name, age, mob_phone, email. Додати методи get_contact, sent_message.
# Створити клас-нащадок UpdateContact з полями surname, name, age, mob_phone, email, job. Додати методи get_message.
# Створити екземпляри класів та дослідити стан об'єктів за допомогою атрибутів: __dict__, __base__, __bases__.
# Роздрукувати інформацію на екрані.


class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return (
            f"{self.name} {self.surname}, {self.age} - {self.mob_phone}, {self.email}"
        )

    @staticmethod
    def send_message(self, message):
        self.message = message
        print(
            f"Sending a message to {self.name} {self.surname}, phone number:{self.mob_phone}, message:{message}"
        )


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self):
        return (
            f"Hello {self.name}, I would like to express my sincere gratitude for your application for the position "
            f"of {self.job}!"
        )


# Object instances creation
contact_1 = Contact("Jones", "Simon", 20, "+1-212-456-7890", "s.jones@gmaIl.com")
contact_2 = UpdateContact(
    "Hannes",
    "Marisa",
    29,
    "+1-212-349-2121",
    "m.hannes@gmaIl.com",
    "Junior Software Engineer (Python)",
)

# Print of information about the objects
print("Contact:")
print("Attributes:", contact_1.__dict__)
print("Base class:", contact_1.__class__.__base__)
print("Bases:", contact_1.__class__.__bases__)
print("Contact Info:", contact_1.get_contact())

print("\nUpdateContact:")
print("Attributes:", contact_2.__dict__)
print("Base class:", contact_2.__class__.__base__)
print("Bases:", contact_2.__class__.__bases__)
print("Contact Info:", contact_2.get_contact())
print("Job:", contact_2.job)
print("Message:", contact_2.get_message())
