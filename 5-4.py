# Використовуючи код з завдання 2, створити 2 екземпляри обох класів.
# Використати функції isinstance() – для перевірки екземплярів класу (за яким класом створені) та issubclass() –
# для перевірки і визначення класу-нащадка.


class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return (
            f"{self.name} {self.surname}, {self.age} - {self.mob_phone}, {self.email}"
        )

    @staticmethod
    def send_message(self, message):
        self.message = message
        print(
            f"Sending a message to {self.name} {self.surname}, phone number:{self.mob_phone}, message:{message}"
        )


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self):
        return (
            f"Hello {self.name}, I would like to express my sincere gratitude for your application for the position "
            f"of {self.job}!"
        )


# Object instances creat
contact_1 = Contact("Jones", "Simon", 20, "+1-212-456-7890", "s.jones@gmaIl.com")
contact_2 = UpdateContact(
    "Hannes",
    "Marisa",
    29,
    "+1-212-349-2121",
    "m.hannes@gmaIl.com",
    "Junior Software Engineer (Python)",)

# Checking the instance class
print("Contact 1 is an instance of the Contact class.", isinstance(contact_1, Contact))
print("Contact 1 is an instance of the UpdateContact class.", isinstance(contact_1, UpdateContact))
print("Contact 2 is an instance of the Contact class", isinstance(contact_2, Contact))
print("Contact 2 is an instance of the UpdateContact class.", isinstance(contact_2, UpdateContact))

# Check the inherited class
print("Contact is a subclass of UpdateContact.", issubclass(Contact, UpdateContact))
print("UpdateContact is a subclass of Contact.", issubclass(UpdateContact, Contact))
