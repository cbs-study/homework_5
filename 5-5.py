# Використовуючи код завдання 2 надрукуйте у терміналі інформацію, яка міститься у класах Contact та UpdateContact та
# їх екземплярах. Видаліть атрибут job, і знову надрукуйте стан класів та їх екземплярів. Порівняйте їх.
# Зробіть відповідні висновки.


class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return (
            f"{self.name} {self.surname}, {self.age} - {self.mob_phone}, {self.email}"
        )

    @staticmethod
    def send_message(self, message):
        self.message = message
        print(
            f"Sending a message to {self.name} {self.surname}, phone number:{self.mob_phone}, message:{message}"
        )


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self):
        return (
            f"Hello {self.name}, I would like to express my sincere gratitude for your application for the position "
            f"of {self.job}!"
        )


# Object instances creat
contact_1 = Contact("Jones", "Simon", 20, "+1-212-456-7890", "s.jones@gmaIl.com")
contact_2 = UpdateContact(
    "Hannes",
    "Marisa",
    29,
    "+1-212-349-2121",
    "m.hannes@gmaIl.com",
    "Junior Software Engineer (Python)",
)
# Print information about the classes
print("Information about the Contact class:")
print(Contact.__dict__)
print("\nInformation about the UpdateContact class:")
print(UpdateContact.__dict__)

# Print information about the instances
print("\nInformation about contact_1:")
print(contact_1.__dict__)
print("\nInformation about contact_2:")
print(contact_2.__dict__)

# Job attribute removing
delattr(contact_2, 'job')

# Print information about the classes and instances
print("\nThe 'job' attribute after removing:")
print("Information about the Contact class:")
print(Contact.__dict__)
print("\nInformation about the UpdateContact class:")
print(UpdateContact.__dict__)
print("\nInformation about contact_1:")
print(contact_1.__dict__)
print("\nInformation about contact_2:")
print(contact_2.__dict__)