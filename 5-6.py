# Використовуючи код завдання 2 надрукуйте у терміналі всі методи, які містяться у класі Contact та UpdateContact.


class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return (
            f"{self.name} {self.surname}, {self.age} - {self.mob_phone}, {self.email}"
        )

    @staticmethod
    def send_message(self, message):
        self.message = message
        print(
            f"Sending a message to {self.name} {self.surname}, phone number:{self.mob_phone}, message:{message}"
        )


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self):
        return (
            f"Hello {self.name}, I would like to express my sincere gratitude for your application for the position "
            f"of {self.job}!"
        )


# Object instances creat
contact_1 = Contact("Jones", "Simon", 20, "+1-212-456-7890", "s.jones@gmaIl.com")
contact_2 = UpdateContact(
    "Hannes",
    "Marisa",
    29,
    "+1-212-349-2121",
    "m.hannes@gmaIl.com",
    "Junior Software Engineer (Python)",
)

# Print all methods of the Contact class
print("\nMethods of the Contact class:")
print([method for method in dir(Contact) if callable(getattr(Contact, method))])

# Print all methods of the UpdateContact class
print("\nMethods of the UpdateContact class:")
print([method for method in dir(UpdateContact) if callable(getattr(UpdateContact, method))])
