# Використовуючи код з завдання 2, використати функції hasattr(), getattr(), setattr(), delattr().
# Застосувати ці функції до кожного з атрибутів класів, подивитися до чого це призводить.

class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return (
            f"{self.name} {self.surname}, {self.age} - {self.mob_phone}, {self.email}"
        )

    @staticmethod
    def send_message(self, message):
        self.message = message
        print(
            f"Sending a message to {self.name} {self.surname}, phone number:{self.mob_phone}, message:{message}"
        )


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self):
        return (
            f"Hello {self.name}, I would like to express my sincere gratitude for your application for the position "
            f"of {self.job}!"
        )


# Object instances creation
contact_1 = Contact("Jones", "Simon", 20, "+1-212-456-7890", "s.jones@gmaIl.com")
contact_2 = UpdateContact(
    "Hannes",
    "Marisa",
    29,
    "+1-212-349-2121",
    "m.hannes@gmaIl.com",
    "Junior Software Engineer (Python)",
)
# Checking the attribute in the Contact class
print("'Surname' attribute in the Contact class:", hasattr(contact_1, 'surname'))
# Getting the value of the 'surname' attribute
print("Value of 'surname' attribute: ", getattr(contact_1, 'surname'))
# Changing the value of the 'surname' attribute
setattr(contact_1, 'surname', 'Wilson')
print("Changed 'surname' attribute in the Contact class:", contact_1.surname)
# Removing the 'surname' attribute
delattr(contact_1, 'surname')
print("'Surname' attribute in the Contact class after removing:", hasattr(contact_1, 'surname'))

# Create an instance of the UpdateContact class
print("\n'Job' attribute in the UpdateContact class:", hasattr(contact_2, 'job'))

print("Value of 'job' attribute:", getattr(contact_2, 'job'))
# Checking the attribute in the UpdateContact class
setattr(contact_2, 'job', 'Data Engineer')
print("Changed 'job' attribute in the UpdateContact class:", contact_2.job)
# Removing the 'job' attribute
delattr(contact_2, 'job')
print("'Job' attribute in the UpdateContact class after removing:", hasattr(contact_2, 'job'))
